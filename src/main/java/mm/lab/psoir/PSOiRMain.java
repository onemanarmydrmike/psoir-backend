package mm.lab.psoir;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class PSOiRMain {

    private static final Logger LOGGER = Logger.getLogger(PSOiRMain.class);

    public static void main(String[] args) {
        SpringApplication.run(PSOiRMain.class, args);
        LOGGER.info("Application started");
    }
}
