package mm.lab.psoir.model;

public enum Action {

    ROTATE,
    GRAYSCALE
}
