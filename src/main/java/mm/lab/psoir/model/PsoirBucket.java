package mm.lab.psoir.model;

import java.util.List;

public class PsoirBucket {

    private String name;
    private List<PsoirFile> files;

    public PsoirBucket(String name, List<PsoirFile> files) {
        this.name = name;
        this.files = files;
    }

    public String getName() {
        return name;
    }

    public List<PsoirFile> getFiles() {
        return files;
    }
}
