package mm.lab.psoir.model;

import java.net.URI;

public class PsoirFile {

    private String name;
    private URI uri;

    public PsoirFile(String name) {
        this.name = name;
    }

    public PsoirFile(String name, URI uri) {
        this.name = name;
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public URI getUri() {
        return uri;
    }
}
