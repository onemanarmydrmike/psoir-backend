package mm.lab.psoir.model;

import java.io.Serializable;

public class SQSMessage implements Serializable {

    private String bucketName;
    private String action;
    private String files;

    public SQSMessage() {
    }

    public SQSMessage(String bucketName, String action, String files) {
        this.bucketName = bucketName;
        this.action = action;
        this.files = files;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getFiles() {
        return files;
    }

    public void setFiles(String files) {
        this.files = files;
    }
}
