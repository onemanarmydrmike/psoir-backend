package mm.lab.psoir.service;

import com.amazonaws.services.sqs.model.Message;
import java.util.List;
import mm.lab.psoir.model.Action;

public interface SQSService extends AWSService {

    void push(String bucketName, String[] files, Action action);

    List<Message> getMessages();

    void deleteMessage(Message message);
}
