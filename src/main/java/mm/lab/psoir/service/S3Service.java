package mm.lab.psoir.service;

import java.net.URL;
import java.util.List;
import mm.lab.psoir.exception.AWSException;
import mm.lab.psoir.exception.BucketNotFoundException;
import mm.lab.psoir.exception.PSOIRException;
import mm.lab.psoir.model.PsoirBucket;

public interface S3Service extends AWSService {

    List<String> listBuckets();

    List<PsoirBucket> getBuckets();

    PsoirBucket getBucket(String bucketName) throws BucketNotFoundException;

    URL preSignUpload(String bucketName, String fileName) throws PSOIRException;

    URL preSignDownload(String bucketName, String fileName) throws PSOIRException;

    void deleteFiles(String bucketName, String[] files) throws AWSException;

    void rotateFiles(String bucketName, String[] files) throws PSOIRException;

    void grayscaleFiles(String bucketName, String[] files) throws PSOIRException;

    void rotateAndUpload(String bucketName, String[] files);

    void grayscaleAndUpload(String bucketName, String[] files);
}
