package mm.lab.psoir.service;

import com.amazonaws.regions.Regions;

public interface AWSService {

    Regions MY_REGION = Regions.EU_WEST_2;

    String SUPPORTED_BUCKETS_PREFIX = "psoir-pictures";
}
