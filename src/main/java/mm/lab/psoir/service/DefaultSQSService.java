package mm.lab.psoir.service;

import static mm.lab.psoir.utils.PSOiRConstants.FILES_SEPARATOR;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.util.List;
import mm.lab.psoir.exception.QueueNotFoundException;
import mm.lab.psoir.model.Action;
import mm.lab.psoir.model.SQSMessage;
import org.springframework.stereotype.Service;

@Service
public class DefaultSQSService implements SQSService {

    private static final String DEFAULT_QUEUE_NAME = "PSOIRQueue.fifo";
    private static final String DEFAULT_MESSAGE_GROUP_ID = "PSOiR";

    private final AmazonSQS sqs;
    private final String defaultQueueUrl;
    private final Gson gson;

    public DefaultSQSService() throws QueueNotFoundException {
        this.sqs = AmazonSQSClient.builder().withRegion(MY_REGION).build();
        this.defaultQueueUrl = getDefaultQueueUrl();
        this.gson = new Gson();
    }

    @Override
    public void push(String bucketName, String[] files, Action action) {
        String concatenatedFiles = String.join(FILES_SEPARATOR, files);
        String actionString = action.toString();

        SQSMessage sqsMessage = new SQSMessage(bucketName, actionString, concatenatedFiles);
        JsonElement messageBody = gson.toJsonTree(sqsMessage, SQSMessage.class);

        SendMessageRequest sendMessageRequest = new SendMessageRequest()
            .withQueueUrl(defaultQueueUrl)
            .withMessageGroupId(DEFAULT_MESSAGE_GROUP_ID)
            .withMessageBody(messageBody.toString());

        sqs.sendMessage(sendMessageRequest);
    }

    @Override
    public List<Message> getMessages() {
        return sqs.receiveMessage(defaultQueueUrl).getMessages();
    }

    @Override
    public void deleteMessage(Message message) {
        sqs.deleteMessage(defaultQueueUrl, message.getReceiptHandle());
    }

    private String getDefaultQueueUrl() throws QueueNotFoundException {
        return sqs.listQueues().getQueueUrls().stream()
            .filter(url -> url.contains(DEFAULT_QUEUE_NAME))
            .findFirst()
            .orElseThrow(() -> new QueueNotFoundException(DEFAULT_QUEUE_NAME));
    }
}
