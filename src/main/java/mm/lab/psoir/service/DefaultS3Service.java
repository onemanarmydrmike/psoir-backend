package mm.lab.psoir.service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.imageio.ImageIO;
import mm.lab.psoir.exception.AWSException;
import mm.lab.psoir.exception.BucketNotFoundException;
import mm.lab.psoir.exception.PSOIRException;
import mm.lab.psoir.model.Action;
import mm.lab.psoir.model.PsoirBucket;
import mm.lab.psoir.model.PsoirFile;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultS3Service implements S3Service {

    private static final Logger LOGGER = Logger.getLogger(DefaultS3Service.class);

    private static final String HOME_TEMP_DIR = "/home/michalm/temp/";
    private static final int MINUTES_30 = 1000 * 60 * 30;

    private final AmazonS3 s3;
    private final SQSService sqsService;

    @Autowired
    public DefaultS3Service(SQSService sqsService) {
        this.s3 = AmazonS3Client.builder()
            .withRegion(MY_REGION)
            .build();
        this.sqsService = sqsService;
    }

    @Override
    public List<String> listBuckets() {
        return streamSupportedBuckets()
            .map(Bucket::getName)
            .collect(Collectors.toList());
    }

    @Override
    public List<PsoirBucket> getBuckets() {
        return streamSupportedBuckets()
            .map(this::mapToApiBucket)
            .collect(Collectors.toList());
    }

    @Override
    public PsoirBucket getBucket(String bucketName) throws BucketNotFoundException {
        Bucket bucket = streamSupportedBuckets()
            .filter(b -> b.getName().equals(bucketName))
            .findAny()
            .orElseThrow(() -> new BucketNotFoundException(bucketName));
        return mapToApiBucket(bucket);
    }

    @Override
    public URL preSignUpload(String bucketName, String fileName) throws PSOIRException {
        try {
            GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, fileName)
                .withMethod(HttpMethod.PUT)
                .withExpiration(new Date(System.currentTimeMillis() + MINUTES_30));
            return s3.generatePresignedUrl(generatePresignedUrlRequest);
        } catch (AmazonServiceException e) {
            throw new PSOIRException(e.getMessage(), e);
        }
    }

    @Override
    public URL preSignDownload(String bucketName, String fileName) throws PSOIRException {
        try {
            GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, fileName)
                .withMethod(HttpMethod.GET)
                .withExpiration(new Date(System.currentTimeMillis() + MINUTES_30));
            return s3.generatePresignedUrl(generatePresignedUrlRequest);
        } catch (AmazonServiceException e) {
            throw new PSOIRException(e.getMessage(), e);
        }
    }

    private void uploadFile(String bucketName, Path path) throws IOException {
        s3.putObject(bucketName, path.getFileName().toString(), path.toFile());
        Files.delete(path);
    }

    @Override
    public void deleteFiles(String bucketName, String[] files) throws AWSException {
        DeleteObjectsRequest request = new DeleteObjectsRequest(bucketName).withKeys(files);
        try {
            s3.deleteObjects(request);
        } catch (SdkClientException e) {
            throw new AWSException(e.getMessage(), e);
        }
    }

    @Override
    public void rotateFiles(String bucketName, String[] files) {
        sqsService.push(bucketName, files, Action.ROTATE);
    }

    @Override
    public void grayscaleFiles(String bucketName, String[] files) {
        sqsService.push(bucketName, files, Action.GRAYSCALE);
    }

    @Override
    public void rotateAndUpload(String bucketName, String[] files) {
        for (String fileName : files) {
            rotateAndUpload(bucketName, fileName);
        }
    }

    private void rotateAndUpload(String bucketName, String fileName) {
        LOGGER.info("rotating file " + fileName);

        try {
            Path pathToConvertedFile = rotate(bucketName, fileName);
            uploadFile(bucketName, pathToConvertedFile);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

        LOGGER.info("rotation completed");
    }

    private Path rotate(String bucketName, String fileName) throws IOException {
        S3ObjectInputStream inputStream = s3.getObject(bucketName, fileName).getObjectContent();
        String path = HOME_TEMP_DIR + fileName;

        Files.copy(inputStream, Paths.get(path));
        File file = new File(path);
        BufferedImage img = ImageIO.read(file);

        // rotation
        final double rads = Math.toRadians(90);
        final double sin = Math.abs(Math.sin(rads));
        final double cos = Math.abs(Math.cos(rads));
        final int w = (int) Math.floor(img.getWidth() * cos + img.getHeight() * sin);
        final int h = (int) Math.floor(img.getHeight() * cos + img.getWidth() * sin);
        BufferedImage rotatedImage = new BufferedImage(w, h, img.getType());
        final AffineTransform at = new AffineTransform();
        at.translate((double) w / 2, (double) h / 2);
        at.rotate(rads, 0, 0);
        at.translate((double) -img.getWidth() / 2, (double) -img.getHeight() / 2);
        final AffineTransformOp rotateOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
        rotatedImage = rotateOp.filter(img, rotatedImage);

        ImageIO.write(rotatedImage, getFileExtension(fileName), file);
        inputStream.close();
        return file.toPath();
    }

    @Override
    public void grayscaleAndUpload(String bucketName, String[] files) {
        for (String fileName : files) {
            grayscaleAndUpload(bucketName, fileName);
        }
    }

    private void grayscaleAndUpload(String bucketName, String fileName) {
        LOGGER.info("grayscaling file " + fileName);

        try {
            Path pathToConvertedFile = grayscale(bucketName, fileName);
            uploadFile(bucketName, pathToConvertedFile);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

        LOGGER.info("grayscaling completed");
    }

    private Path grayscale(String bucketName, String fileName) throws IOException {
        S3ObjectInputStream inputStream = s3.getObject(bucketName, fileName).getObjectContent();
        String path = HOME_TEMP_DIR + fileName;

        Files.copy(inputStream, Paths.get(path));
        File file = new File(path);
        BufferedImage img = ImageIO.read(file);

        // convert to grayscale
        int width = img.getWidth();
        int height = img.getHeight();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int p = img.getRGB(x, y);
                int a = (p >> 24) & 0xff;
                int r = (p >> 16) & 0xff;
                int g = (p >> 8) & 0xff;
                int b = p & 0xff;
                int avg = (r + g + b) / 3;
                p = (a << 24) | (avg << 16) | (avg << 8) | avg;
                img.setRGB(x, y, p);
            }
        }
        ImageIO.write(img, getFileExtension(fileName), file);
        inputStream.close();
        return file.toPath();
    }

    private String getFileExtension(String fileName) {
        return fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length());
    }

    private Stream<Bucket> streamSupportedBuckets() {
        return s3.listBuckets().stream()
            .filter(bucket -> bucket.getName().startsWith(SUPPORTED_BUCKETS_PREFIX));
    }

    private PsoirBucket mapToApiBucket(Bucket bucket) {
        return new PsoirBucket(bucket.getName(), getFilesFromTheBucket(bucket));
    }

    private PsoirFile mapToApiFile(S3ObjectSummary s3ObjectSummary) {
        String fileName = s3ObjectSummary.getKey();
        URI uri = getFileUri(s3ObjectSummary.getBucketName(), fileName);
        return new PsoirFile(fileName, uri);
    }

    private List<PsoirFile> getFilesFromTheBucket(Bucket bucket) {
        ListObjectsRequest request = new ListObjectsRequest().withBucketName(bucket.getName());
        return s3.listObjects(request).getObjectSummaries().stream()
            .filter(file -> !file.getKey().endsWith("/"))
            .map(this::mapToApiFile)
            .collect(Collectors.toList());
    }

    private URI getFileUri(String bucketName, String fileName) {
        GetObjectRequest request = new GetObjectRequest(bucketName, fileName);
        return s3.getObject(request).getObjectContent().getHttpRequest().getURI();
    }
}
