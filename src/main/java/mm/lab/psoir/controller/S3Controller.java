package mm.lab.psoir.controller;

import com.google.gson.Gson;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import mm.lab.psoir.exception.AWSException;
import mm.lab.psoir.exception.BucketNotFoundException;
import mm.lab.psoir.exception.PSOIRException;
import mm.lab.psoir.model.PsoirBucket;
import mm.lab.psoir.service.S3Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/s3")
public class S3Controller implements AWSController {

    private final S3Service s3Service;
    private final Gson gson;

    @Autowired
    public S3Controller(S3Service s3Service) {
        this.s3Service = s3Service;
        this.gson = new Gson();
    }

    @RequestMapping("/listBuckets")
    public Collection<String> listBuckets() {
        return s3Service.listBuckets();
    }

    @RequestMapping("/buckets")
    public Collection<PsoirBucket> getBuckets() {
        return s3Service.getBuckets();
    }

    @RequestMapping("/bucket/{name}")
    public PsoirBucket getBucket(@PathVariable("name") final String bucketName) throws BucketNotFoundException {
        return s3Service.getBucket(bucketName);
    }

    @PostMapping("/bucket/{bucketName}/uploadFile")
    public ResponseEntity<String> uploadFile(@PathVariable("bucketName") final String bucketName, @RequestParam("file") final String fileName) {
        try {
            URL preSignedUrl = s3Service.preSignUpload(bucketName, fileName);
            return new ResponseEntity<>(preSignedUrl.toString(), HttpStatus.OK);
        } catch (PSOIRException e) {
            return new ResponseEntity<>("file " + fileName + " has not been uploaded", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/bucket/{bucketName}/deleteFiles")
    public ResponseEntity<String> deleteFiles(@PathVariable("bucketName") final String bucketName, @RequestParam("files") final String[] files) {
        try {
            s3Service.deleteFiles(bucketName, files);
        } catch (AWSException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("files deleted", HttpStatus.OK);
    }

    @PostMapping("/bucket/{bucketName}/downloadFiles")
    public ResponseEntity<String> downloadFiles(@PathVariable("bucketName") final String bucketName, @RequestParam("files") final String[] files) {
        try {
            List<String> preSignedUrls = new ArrayList<>();
            for (String fileName : files) {
                URL preSignedUrl = s3Service.preSignDownload(bucketName, fileName);
                preSignedUrls.add(preSignedUrl.toString());
            }
            return new ResponseEntity<>(gson.toJson(preSignedUrls.toArray()), HttpStatus.OK);
        } catch (PSOIRException e) {
            return new ResponseEntity<>("file " + files[0] + " has not been downloaded", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/bucket/{bucketName}/rotateFiles")
    public ResponseEntity<String> rotateFiles(@PathVariable("bucketName") final String bucketName, @RequestParam("files") final String[] files) {
        try {
            s3Service.rotateFiles(bucketName, files);
        } catch (PSOIRException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Rotation sent to queue", HttpStatus.OK);
    }

    @PostMapping("/bucket/{bucketName}/grayscaleFiles")
    public ResponseEntity<String> grayscaleFiles(@PathVariable("bucketName") final String bucketName, @RequestParam("files") final String[] files) {
        try {
            s3Service.grayscaleFiles(bucketName, files);
        } catch (PSOIRException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Grayscaling sent to queue", HttpStatus.OK);
    }
}
