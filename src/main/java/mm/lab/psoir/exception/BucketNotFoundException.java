package mm.lab.psoir.exception;

public class BucketNotFoundException extends AWSException {

    private static final String MESSAGE = "Bucket '%s' not found";

    public BucketNotFoundException(String bucketName) {
        super(String.format(MESSAGE, bucketName));
    }

    public BucketNotFoundException(String bucketName, Throwable throwable) {
        super(String.format(MESSAGE, bucketName), throwable);
    }
}
