package mm.lab.psoir.exception;

public class QueueNotFoundException extends AWSException {

    private static final String MESSAGE = "Queue '%s' not found";

    public QueueNotFoundException(String queueName) {
        super(String.format(MESSAGE, queueName));
    }

    public QueueNotFoundException(String queueName, Throwable throwable) {
        super(String.format(MESSAGE, queueName), throwable);
    }
}
