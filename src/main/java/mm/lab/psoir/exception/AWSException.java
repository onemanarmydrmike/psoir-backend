package mm.lab.psoir.exception;

public class AWSException extends PSOIRException {

    public AWSException(String message) {
        super(message);
    }

    public AWSException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
