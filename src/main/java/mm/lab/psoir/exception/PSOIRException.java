package mm.lab.psoir.exception;

public class PSOIRException extends Exception {

    public PSOIRException(String message) {
        super(message);
    }

    public PSOIRException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
