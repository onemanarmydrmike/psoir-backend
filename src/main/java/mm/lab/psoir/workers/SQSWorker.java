package mm.lab.psoir.workers;

import com.amazonaws.services.sqs.model.Message;
import com.google.gson.Gson;
import java.util.List;
import mm.lab.psoir.model.Action;
import mm.lab.psoir.model.SQSMessage;
import mm.lab.psoir.service.S3Service;
import mm.lab.psoir.service.SQSService;
import mm.lab.psoir.utils.PSOiRConstants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SQSWorker {

    private static final Logger LOGGER = Logger.getLogger(SQSWorker.class);

    private final SQSService sqsService;
    private final S3Service s3Service;

    private final Gson gson = new Gson();

    @Autowired
    public SQSWorker(SQSService sqsService, S3Service s3Service) {
        this.sqsService = sqsService;
        this.s3Service = s3Service;
    }

    @Scheduled(fixedRate = 3000, initialDelay = 1000)
    public void pop() {
        List<Message> messages = sqsService.getMessages();
        for (Message message : messages) {
            try {
                doTheAction(message);
                sqsService.deleteMessage(message);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    private void doTheAction(Message message) {
        SQSMessage sqsMessage = gson.fromJson(message.getBody(), SQSMessage.class);
        String bucketName = sqsMessage.getBucketName();
        String[] files = sqsMessage.getFiles().split(PSOiRConstants.FILES_SEPARATOR);

        String action = sqsMessage.getAction();
        if (action.equalsIgnoreCase(Action.ROTATE.toString())) {
            rotate(bucketName, files);
        } else if (action.equalsIgnoreCase(Action.GRAYSCALE.toString())) {
            grayscale(bucketName, files);
        }
    }

    private void rotate(String bucketName, String[] files) {
        s3Service.rotateAndUpload(bucketName, files);
    }

    private void grayscale(String bucketName, String[] files) {
        s3Service.grayscaleAndUpload(bucketName, files);
    }


}
