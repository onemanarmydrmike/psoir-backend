# PSOiRLab

Projektowanie Systemów Obiektowych i Rozproszonych - Project created for university purpose

## Wymagania funkcjonalne:
### Celem projektu jest opracowanie systemu pozwalającego na przechowywanie i przetwarzanie plików graficznych. System powinien pozwalać na:
1. Zapisywanie plików graficznych
2. Przeglądanie listy zapisanych w systemie plików graficznych, (oraz opcjonalnie ich pobieranie oraz usuwanie).
3. Wybór grupy plików graficznych zapisanych w systemie i zlecenie wykonania na nich przykładowej transformacji graficznej (np. obrót, skalowanie, itp.). Ważne jest aby zlecona operacja mogła być wykonana na grupie (wsadzie - ang. batch) plików.  

## Ograniczenia:
1. System powinien skalować się wraz ze wzrostem obciążenia
2. System powinien być odporny na awarie
